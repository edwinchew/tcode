import unittest
import re, copy
from datetime import datetime
from decimal import Decimal, ROUND_HALF_UP

class Transaction(object):
    """ Splits a string, performs transformations and checks on it """

    def __init__(self, transact_str):
        hashedcc_str, timestamp_str, price_str = \
            __class__._split_string_to_components(transact_str)
        self.hashedcc = hashedcc_str
        self.timestamp = timestamp_str
        self.price = price_str

    @staticmethod
    def _split_string_to_components(transact_str, size=3):
        return [ item.strip() for item in transact_str.split(',') ][:size]

    @staticmethod
    def _timestamp_string_to_datetime(timestamp_str, timestamp_fmt='%Y-%m-%dT%H:%M:%S'):
        return datetime.strptime(timestamp_str, timestamp_fmt)

    @staticmethod
    def _price_string_to_decimal(price_str, exp=Decimal('0.01'), rounding=ROUND_HALF_UP):
        return Decimal(price_str).quantize(exp, rounding)

    @property
    def hashedcc(self):
        return self._hashedcc

    @hashedcc.setter
    def hashedcc(self, raw):
        if type(raw) is not str:
            raise TypeError('Hashed CC number must be string')
        if not re.search('^([0-9a-fA-F]+)$', raw):
            raise TypeError('Hashed CC number char must be HEX')
        # TBD: Should we do length check?
        self._hashedcc = raw

    @property
    def timestamp(self):
        if len(self._timestamp) == 1:
            return self._timestamp[0]
        else:
            return self._timestamp

    @timestamp.setter
    def timestamp(self, raw):
        self._timestamp = [ __class__._timestamp_string_to_datetime(raw) ]

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, raw):
        self._price = __class__._price_string_to_decimal(raw)

    def __str__(self):
        ts = ','.join(x.strftime('%Y-%m-%dT%H:%M:%S') for x in self._timestamp)
        if len(self._timestamp) > 1:
            ts = '[{}]'.format(ts)
        return '{HC}, {TS}, {PR}'.format(HC=self.hashedcc, TS=ts, PR=self.price)

    def is_date(self, date_str, datestamp_fmt='%Y-%m-%d'):
        return all(x.strftime(datestamp_fmt) == date_str for x in self._timestamp)

    def copy(self):
        return copy.deepcopy(self)

    def __add__(self, other):
        if type(other) is not type(self):
            raise TypeError('Operand must be type Transaction')
        if self.hashedcc != other.hashedcc:
            raise Exception('Operand must have the same Hashed CC Number')
        copy = self.copy()
        copy._timestamp.extend(other._timestamp)
        copy._price += other._price
        return copy

class testcase_Transaction(unittest.TestCase):
    """ Test suite for Transaction class """

    def setUp(self):
        self.hashedcc_str = '10d7ce2f43e35fa57d1bbf8b1e2'
        self.timestamp_str = '2014-04-29T13:15:54'
        self.price_str = '10.00'
        self.transact_str = '{HC}, {TS}, {PR}'.format(HC=self.hashedcc_str, 
            TS=self.timestamp_str, PR=self.price_str)
        self.trxn = Transaction(self.transact_str)

    def tearDown(self):
        self.trxn = None

    def test__split_string_to_components(self):
        results = Transaction._split_string_to_components(self.transact_str)
        self.assertEqual(results[0], self.hashedcc_str) 
        self.assertEqual(results[1], self.timestamp_str) 
        self.assertEqual(results[2], self.price_str) 

    def test__timestamp_string_to_datetime(self):
        results = Transaction._timestamp_string_to_datetime(self.timestamp_str)
        self.assertEqual(results, datetime(2014,4,29,13,15,54)) 

    def test__price_string_to_decimal(self):
        self.assertEqual(Transaction._price_string_to_decimal('10.00'), Decimal('10.00'))
        self.assertEqual(Transaction._price_string_to_decimal('9.999'), Decimal('10.00'))
        self.assertEqual(Transaction._price_string_to_decimal('9.995'), Decimal('10.00'))
        self.assertEqual(Transaction._price_string_to_decimal('9.994'), Decimal('9.99'))

    def test__init(self):
        self.assertEqual(self.trxn.hashedcc, self.hashedcc_str)
        self.assertEqual(self.trxn.timestamp, datetime(2014,4,29,13,15,54))
        self.assertEqual(self.trxn.price, Decimal(self.price_str))

    def test_copy(self):
        copy_trxn = self.trxn.copy()
        self.assertIsNot(copy_trxn, self.trxn)
        self.assertEqual(str(copy_trxn.hashedcc), str(self.trxn.hashedcc))
        self.assertEqual(str(copy_trxn.timestamp), str(self.trxn.timestamp))
        self.assertEqual(str(copy_trxn.price), str(self.trxn.price))

    def test__add(self):
        trxn2 = self.trxn + self.trxn
        self.assertIsNot(trxn2, self.trxn) # Should be a copy!
        self.assertEqual(trxn2.hashedcc, self.trxn.hashedcc)
        dt = datetime(2014,4,29,13,15,54)
        self.assertListEqual(trxn2.timestamp, [dt, dt])
        self.assertEqual(trxn2.price, Decimal('20.00'))

    def test_is_date(self):
        self.assertTrue(self.trxn.is_date('2014-04-29'))
        self.assertFalse(self.trxn.is_date('2014-04-19'))
        self.trxn._timestamp.append(datetime(2014,4,29,0,0,0))
        self.assertTrue(self.trxn.is_date('2014-04-29'))
        self.trxn._timestamp.append(datetime(2014,4,28,13,15,54))
        self.assertFalse(self.trxn.is_date('2014-04-29'))

    def test__str(self):
        self.assertEqual(str(self.trxn), self.transact_str)
        self.trxn._timestamp.append(datetime(2014,4,29,0,0,0))
        new_str = '{HC}, [{TS1},{TS2}], {PR}'.format(HC=self.hashedcc_str, 
            TS1=self.timestamp_str, TS2='2014-04-29T00:00:00', PR=self.price_str)
        self.assertEqual(str(self.trxn), new_str)

if __name__ == '__main__':
    unittest.main()
