import unittest, testfixtures
from itertools import groupby
from functools import reduce
from transaction import Transaction
from decimal import Decimal
from datetime import datetime

class Transform(object):
    """ Sort, Map, Filter and Reduce the transactions """

    @staticmethod
    def _map_string_list_to_iterable(str_list, item_func):
        return ( item_func(item_str) for item_str in str_list )

    @staticmethod
    def _filter_iterable_by_function(iterable, filter_func):
        return ( item for item in iterable if filter_func(item) )

    @staticmethod
    def _sort_iterable_by_function(iterable, key_func):
        return iter( sorted([item for item in iterable], key=key_func) )

    @staticmethod
    def _group_reduce_iterable_by_functions(iterable, group_func, reduce_func):
        # Groupby requires that the input iterable has already been sorted by key
        return ( reduce(reduce_func, group) for key, group in groupby(iterable, key=group_func) )

    @staticmethod
    def _map_transact_str_list_to_iterable(transact_str_list):
        return __class__._map_string_list_to_iterable(transact_str_list, Transaction)

class TransformTransaction(Transform):

    @staticmethod
    def _transaction_date_filter(date):
        return lambda transaction: transaction.is_date(date) 

    @staticmethod
    def _filter_transactions_by_date(transactions_iterable, date):
        filter_date = __class__._transaction_date_filter(date)
        return __class__._filter_iterable_by_function(transactions_iterable, filter_date)

    @staticmethod
    def _transaction_price_filter(price):
        threshold = Transaction._price_string_to_decimal(price)
        return lambda transaction: transaction.price > threshold

    @staticmethod
    def _filter_transactions_greater_than_price_threshold(transactions_iterable, price):
        filter_price = __class__._transaction_price_filter(price)
        return __class__._filter_iterable_by_function(transactions_iterable, filter_price)

    @staticmethod
    def _transaction_key_function(attribute):
        return lambda item: getattr(item, attribute)

    @staticmethod
    def _sort_transactions_iterable_by_hashedcc(transactions_iterable):
        sort_hashedcc = __class__._transaction_key_function('hashedcc')
        return __class__._sort_iterable_by_function(transactions_iterable, sort_hashedcc)

    @staticmethod
    def _transaction_reduce_function():
        return lambda left, right: left + right

    @staticmethod
    def _reduce_prices_in_grouped_transactions(transactions_iterable):
        group_hashedcc = __class__._transaction_key_function('hashedcc')
        reduce_price = __class__._transaction_reduce_function()
        return __class__._group_reduce_iterable_by_functions(
            transactions_iterable, group_hashedcc, reduce_price)

    @staticmethod
    def expose_fraudulent_transactions(transact_str_list, date, price_threshold):
        tx0 = __class__._map_transact_str_list_to_iterable(transact_str_list)
        tx1 = __class__._filter_transactions_by_date(tx0, date)
        tx2 = __class__._sort_transactions_iterable_by_hashedcc(tx1)
        tx3 = __class__._reduce_prices_in_grouped_transactions(tx2)
        tx4 = __class__._filter_transactions_greater_than_price_threshold(tx3, price_threshold)
        return [ transaction.hashedcc for transaction in tx4 ]

class testcase_TransformTransaction(unittest.TestCase):
    """ Testcases for the TransformTransaction class """

    def setUp(self):
        self.transact_str_list = [
            'A1, 2017-01-01T01:00:00, 0.99',
            'B2, 2017-01-01T02:00:00, 1.00',
            'C3, 2017-01-01T03:00:00, 1.00',
            'A1, 2017-01-02T05:00:00, 1.00',
            'A1, 2017-01-02T04:00:00, 1.01',
            'C3, 2017-01-02T06:00:00, 1.01',
        ]

    def test__map_transact_str_list_to_iterable(self):
        timestamp_fmt = '%Y-%m-%dT%H:%M:%S'
        results = ( str(trxn) for trxn in \
            TransformTransaction._map_transact_str_list_to_iterable(self.transact_str_list) )
        self.assertEqual(next(results), self.transact_str_list[0], 1)
        self.assertEqual(next(results), self.transact_str_list[1], 2)
        self.assertEqual(next(results), self.transact_str_list[2], 3)
        self.assertEqual(next(results), self.transact_str_list[3], 4)
        self.assertEqual(next(results), self.transact_str_list[4], 5)
        self.assertEqual(next(results), self.transact_str_list[5], 6)
        self.assertRaises(StopIteration, next, results) 

    def test__sort_transactions_iterable_by_hashedcc(self):
        trxn_iter = ( Transaction(x) for x in self.transact_str_list )
        results = ( str(trxn) for trxn in \
            TransformTransaction._sort_transactions_iterable_by_hashedcc(trxn_iter) )
        self.assertEqual(next(results), self.transact_str_list[0], 1)
        self.assertEqual(next(results), self.transact_str_list[3], 2)
        self.assertEqual(next(results), self.transact_str_list[4], 3)
        self.assertEqual(next(results), self.transact_str_list[1], 4)
        self.assertEqual(next(results), self.transact_str_list[2], 5)
        self.assertEqual(next(results), self.transact_str_list[5], 6)
        self.assertRaises(StopIteration, next, results) 

    def test__filter_transactions_by_date(self):
        # This is messy, should mock instead!
        trxn_yes = Transaction('A1, 2017-01-23T12:34:56, 1.23')
        trxn_no = Transaction('F9, 2017-12-30T01:23:45, 6.78')
        results = TransformTransaction._filter_transactions_by_date(
            [trxn_yes, trxn_no], '2017-01-23')
        self.assertTrue(testfixtures.Comparison(next(results)) == trxn_yes, 1)
        self.assertRaises(StopIteration, next, results)
        # Switch up the order
        results = TransformTransaction._filter_transactions_by_date(
            [trxn_no, trxn_yes], '2017-01-23')
        self.assertTrue(testfixtures.Comparison(next(results)) == trxn_yes, 2)
        self.assertRaises(StopIteration, next, results)

    def test__reduce_prices_in_grouped_transactions(self):
        # This is messy, should mock instead!
        grp1_trxn1 = Transaction('A1, 2017-01-23T12:34:56, 0.33')
        grp1_trxn2 = Transaction('A1, 2017-01-23T23:45:01, 0.67')
        grp2_trxn1 = Transaction('B2, 2017-01-23T12:34:56, 0.50')
        grp1_accres = Transaction('A1, 2017-01-23T12:34:56, 1.00')
        grp1_accres._timestamp.append(datetime(2017,1,23,23,45,1)) # Naughty!
        results = TransformTransaction._reduce_prices_in_grouped_transactions(
            [grp1_trxn1, grp1_trxn2, grp2_trxn1])
        self.assertTrue(testfixtures.Comparison(next(results)) == grp1_accres, 1)
        self.assertTrue(testfixtures.Comparison(next(results)) == grp2_trxn1, 2)
        self.assertRaises(StopIteration, next, results)

    def test__filter_transactions_greater_than_price_threshold(self):
        # This is messy, should mock instead!
        trxn1 = Transaction('A1, 2017-01-23T12:34:56, 0.33')
        trxn2 = Transaction('A2, 2017-01-23T12:34:56, 0.67')
        trxn3 = Transaction('A3, 2017-01-23T12:34:56, 0.50')
        results = TransformTransaction._filter_transactions_greater_than_price_threshold(
            [trxn1, trxn2, trxn3], Decimal('0.50'))
        self.assertEqual(next(results), trxn2, 1)
        self.assertRaises(StopIteration, next, results)
        results = TransformTransaction._filter_transactions_greater_than_price_threshold(
            [trxn3, trxn2, trxn1], Decimal('0.50'))
        self.assertEqual(next(results), trxn2, 2)
        self.assertRaises(StopIteration, next, results)
        pass

    def test_expose_fraudulent_transactions(self):
        # w00t? This is more like a system test!
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-02', '2.01')
        self.assertListEqual(results, [], 1)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-02', '2.00')
        self.assertListEqual(results, ['A1'], 2)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-02', '1.01')
        self.assertListEqual(results, ['A1'], 3)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-02', '1.00')
        self.assertListEqual(results, ['A1', 'C3'], 4)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-01', '1.00')
        self.assertListEqual(results, [], 5)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-01', '0.99')
        self.assertListEqual(results, ['B2', 'C3'], 6)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-01-01', '0.98')
        self.assertListEqual(results, ['A1', 'B2', 'C3'], 7)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-12-30', '1.00')
        self.assertListEqual(results, [], 8)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-12-30', '0.99')
        self.assertListEqual(results, [], 9)
        results = TransformTransaction.expose_fraudulent_transactions(
            self.transact_str_list, '2017-12-30', '0.00')
        self.assertListEqual(results, [], 10)

if __name__ == '__main__':
    unittest.main()
